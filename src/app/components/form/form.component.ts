import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  form!: FormGroup;
  name: string[] = []
  nombres!: string;

  constructor(private fb: FormBuilder) { 
    this.crearForm();
  }

  
  get pasatiemposO() {
    return this.form.get('pasatiempos') as FormArray;
    }

  ngOnInit(): void {
  }  
  
  crearForm(): void{
    this.form = this.fb.group({
      nombre: ['', Validators.required],
      pasatiempos: this.fb.array([])
    })
  }

  adicionar(): void{
    console.log('ADICIONAR');
    console.log(this.form.value.nombre);
    this.name.push(this.form.value.nombre)
    this.pasatiemposO.push(this.fb.control(''))
    this.form.value.nombre = ''
   }

   borrarCaja(i: number): void{
    this.pasatiemposO.removeAt(i);
  }

  vaciarcaja():void {
    this.nombres = ''
  }

  guardar(): void{
    this.nombres = 'Nombres:' + this.name;
  }
}
